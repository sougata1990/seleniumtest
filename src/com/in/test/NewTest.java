package com.in.test;

import org.testng.annotations.Test;

import uimap.HomePage;

import org.testng.annotations.BeforeTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;

public class NewTest {
	WebDriver driver;
	Properties props;

	@Test(priority = 0)
	public void f() throws IOException {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(file, new File("D:\\test.png"));
	}

	@Test(priority = 1)
	public void register() throws IOException, InterruptedException {
		driver.findElement(HomePage.RegisterLink).click();
		new ReusableFunctions().Register();
	}

	@BeforeTest
	public void beforeTest() throws IOException {
		props = new Properties();
		InputStream fis = new FileInputStream("config.properties");
		props.load(fis);
		System.setProperty("webdriver.chrome.driver", props.getProperty("ChromeDriver"));
		driver = new ChromeDriver();
		driver.get(props.getProperty("URL"));
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
