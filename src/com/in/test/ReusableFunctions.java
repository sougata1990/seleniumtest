package com.in.test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.support.ui.Select;

import uimap.RegisterPage;

public class ReusableFunctions extends NewTest{
	public String readFromExcel(int rowNum, int cellNum) throws IOException {
		FileInputStream fis = new FileInputStream("C:\\Users\\sougata das\\Desktop\\Test.xlsx");
		HSSFWorkbook wb = new HSSFWorkbook(fis);
		HSSFSheet sh = wb.getSheetAt(0);
		HSSFRow row = sh.getRow(rowNum);
		HSSFCell cell = row.getCell(cellNum);
		String val = cell.getStringCellValue();

		return val;
	}
	
	public void writeToExcel(int rowNum,int cellNum,String val)throws IOException {
		FileInputStream fis=new FileInputStream("C:\\Users\\sougata das\\Desktop\\Test.xlsx");
		HSSFWorkbook wb=new HSSFWorkbook(fis);
		HSSFSheet sh=wb.getSheetAt(0);
		HSSFRow row=sh.createRow(rowNum);
		HSSFCell cell=row.createCell(cellNum);
		cell.setCellValue(val);
		FileOutputStream fos=new FileOutputStream("C:\\Users\\sougata das\\Desktop\\Test.xlsx");
		wb.write(fos);
		fos.flush();
		fos.close();
		
		
	}
	public void Register() throws IOException{
		String details[]=readFromExcel(1,2).split(",");
		driver.findElement(RegisterPage.firstName).sendKeys(details[0]);
		driver.findElement(RegisterPage.lastName).sendKeys(details[1]);
		driver.findElement(RegisterPage.phone).sendKeys(details[2]);
		driver.findElement(RegisterPage.email).sendKeys(details[3]);
		driver.findElement(RegisterPage.address).sendKeys(details[4]);
		driver.findElement(RegisterPage.city).sendKeys(details[5]);
		driver.findElement(RegisterPage.state).sendKeys(details[6]);
		driver.findElement(RegisterPage.zip).sendKeys(details[7]);
		Select select=new Select(driver.findElement(RegisterPage.country));
		select.selectByValue(details[8]);
		driver.findElement(RegisterPage.userName).sendKeys(details[9]);
		driver.findElement(RegisterPage.password).sendKeys(details[10]);
		driver.findElement(RegisterPage.confirmPassword).sendKeys(details[10]);
		driver.findElement(RegisterPage.registerBtn).sendKeys(details[10]);


		
		
		
	}

}
